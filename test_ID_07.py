from PageObject import Find_element_click
import time

#_Issuer_Sales_Configuration

def test_ID7_Issuer_Sales_Configuration(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.email_artur('a.korotkiy@ive.one')
    # ive_one_main_page.password_artur('Qbug.151?200')
    # ive_one_main_page.log_in_button()
    ive_one_main_page.Open_Autotest_project_button()

    ive_one_main_page.Settings_button()
    ive_one_main_page.Sales_CONF()
    ive_one_main_page.Create_button()
    ive_one_main_page.Retail_investor_button()
    ive_one_main_page.proffesional_investor_button()
    ive_one_main_page.Save_button()
    # ive_one_main_page.popup_settings_saved_check()

    ive_one_main_page.Sale_Configuration_2()
    ive_one_main_page.Min_invest_input()
    ive_one_main_page.Max_invest_input()
    ive_one_main_page.Save_button()

    ive_one_main_page.Payment_Configuration_3()
    ive_one_main_page.Add_new_payment_method_button()
    ive_one_main_page.Currency_droplist()
    ive_one_main_page.EUR()
    ive_one_main_page.Payment_Provider_Type_droplist()
    ive_one_main_page.Bank_transfer()
    ive_one_main_page.Holder_name_input()
    ive_one_main_page.IBAN_input()
    ive_one_main_page.Bank_input()
    ive_one_main_page.BIC_input()
    ive_one_main_page.Bank_address_input()
    ive_one_main_page.Country_input()
    ive_one_main_page.Address_owner_input()
    ive_one_main_page.Save_button()

    ive_one_main_page.Fees_Configuration_4()
    ive_one_main_page.Add_new_fee_button()
    ive_one_main_page.Fixed_fee_name_input()
    ive_one_main_page.Fixed_fee_description_input()
    ive_one_main_page.Fee_Type_droplist()
    ive_one_main_page.Fixed_fee_value_input()
    ive_one_main_page.Save_button()

    ive_one_main_page.File_Configuration_5()
    ive_one_main_page.Add_new_file_button()
    ive_one_main_page.File_name_input()
    ive_one_main_page.File_Language_droplist()
    ive_one_main_page.Upload_file_button()
    ive_one_main_page.Save_button()

    ive_one_main_page.Settings_button()
    ive_one_main_page.Sales_CONF()
    ive_one_main_page.Delete_Configuration_button()
    # ive_one_main_page.popup_Sales_configuration_deleted_check()