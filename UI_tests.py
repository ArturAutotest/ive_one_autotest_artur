from PageObject import Find_element_click, API_tests
import time
import requests

                                                                            #Issuer   

def test_1_login(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    ive_one_main_page.log_in_button()
    ive_one_main_page.email_artur('a.korotkiy@ive.one')
    ive_one_main_page.password_artur('Qbug.151?200')
    ive_one_main_page.log_in_button()
    time.sleep(5)

def test_Issuer_Create_Asset(browser):
    randomSymbol = API_tests()
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    ive_one_main_page.open_tabMenu()
    ive_one_main_page.Assets_tab()
    # ive_one_main_page.Create_new_asset()
    # ive_one_main_page.RealEstate_button()
    # ive_one_main_page.next_button()
    # ive_one_main_page.Asset_name_input()
    # randomSymbol.renderRandomSymbol()
    # ive_one_main_page.Asset_symbol_input(randomSymbol.symbol)
    # ive_one_main_page.Create_button()
    # ive_one_main_page.Project_Settings()
    # ive_one_main_page.Token_Config()
    # ive_one_main_page.TokenAmount_input('555000')
    # ive_one_main_page.TokenNetwork()
    # ive_one_main_page.Token_NameOfSmartContract()
    # ive_one_main_page.Save_button()
    # ive_one_main_page.Deploy_Token()
    # ive_one_main_page.LinkToSmartContract()

    ive_one_main_page.Open_Autotest_project_button()

    
    ive_one_main_page.CapTable_tab()
    ive_one_main_page.Investors_subtab()
    ive_one_main_page.check_ShareholderAmount()
    ive_one_main_page.add_Investor()
    ive_one_main_page.email_field('panemox397@htoal.com')
    ive_one_main_page.firstName_field('Name')
    ive_one_main_page.lastName_field('lastName')
    ive_one_main_page.add_button()
    ive_one_main_page.Trading_tab()
    ive_one_main_page.Orders_tab()
    # ive_one_main_page.enablePrimaryMarket()
    ive_one_main_page.createSellOrder()
    ive_one_main_page.logout()

    ive_one_main_page.go_to_site()
    ive_one_main_page.sign_in_button()
    ive_one_main_page.email_artur('panemox397@htoal.com')
    ive_one_main_page.password_artur('Qwer123!')
    ive_one_main_page.log_in_button()
    ive_one_main_page.open_tabMenu()
    ive_one_main_page.Portfolio_tab()
    ive_one_main_page.refresh_page()
    ive_one_main_page.Invest_to_Autotest()
    ive_one_main_page.payment_methods()
    ive_one_main_page.fiatAccount_method()
    ive_one_main_page.next_button()
    ive_one_main_page.invest_button()
    ive_one_main_page.continue_button()
    ive_one_main_page.check_InvestorOrder()
    ive_one_main_page.logout()

    ive_one_main_page.go_to_issuer()
    ive_one_main_page.log_in_button()
    ive_one_main_page.email_artur('a.korotkiy@ive.one')
    ive_one_main_page.password_artur('Qbug.151?200')
    ive_one_main_page.open_tabMenu()
    ive_one_main_page.log_in_button()
    ive_one_main_page.Open_Autotest_project_button()
    ive_one_main_page.Project_Settings()
    ive_one_main_page.Delete_asset('Autotest')







# def test_ID2_buy_Vogeman_Bank_Transfer(browser):
#     ive_one_main_page = Find_element_click(browser)
#     ive_one_main_page.go_to_site()
#     ive_one_main_page.invest_vogeman_green_ship()
#     ive_one_main_page.next_button()
#     ive_one_main_page.purchase_order_confirmations_1()
#     ive_one_main_page.next_button()
#     ive_one_main_page.buy_button()
#     ive_one_main_page.confirm_payment_button()
#     ive_one_main_page.successfully_payment_check()

# def test_ID3_buy_Vogeman_PayPal(browser):
#     ive_one_main_page = Find_element_click(browser)
#     ive_one_main_page.go_to_site()
#     ive_one_main_page.invest_vogeman_green_ship()
#     ive_one_main_page.payment_methods()
#     ive_one_main_page.paypal_method()
#     ive_one_main_page.next_button()
#     ive_one_main_page.purchase_order_confirmations_1()
#     ive_one_main_page.next_button()
#     ive_one_main_page.Paypal_buy_button()
#     ive_one_main_page.switch_page_2()
#     ive_one_main_page.paypal_email_parametrize('sb-d5kus15732531@personal.example.com')
#     ive_one_main_page.paypal_next()
#     ive_one_main_page.paypal_password('P@ssword01')
#     ive_one_main_page.paypal_login()
#     # ive_one_main_page.paypal_X()
#     ive_one_main_page.paypal_pay_now()
#     ive_one_main_page.successfully_payment_paypal_check()

# def test_ID4_buy_Vogeman_CreditCard(browser):
#     ive_one_main_page = Find_element_click(browser)
#     ive_one_main_page.go_to_site()
#     ive_one_main_page.invest_vogeman_green_ship()
#     ive_one_main_page.payment_methods()
#     ive_one_main_page.credit_card_method()
#     ive_one_main_page.next_button()
#     ive_one_main_page.purchase_order_confirmations_1()
#     ive_one_main_page.next_button()
#     ive_one_main_page.next_button()
#     ive_one_main_page.cardholder_name()
#     ive_one_main_page.card_number()
#     ive_one_main_page.card_month_year()
#     ive_one_main_page.card_cvc()
#     ive_one_main_page.pay_button()
#     ive_one_main_page.successfully_payment_check()