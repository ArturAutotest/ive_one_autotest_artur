from PageObject import Find_element_click
import time

#_Issuer_DataRoom

def test_ID10_Issuer_DataRoom(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.email_artur('a.korotkiy@ive.one')
    # ive_one_main_page.password_artur('Qbug.151?200')
    # ive_one_main_page.log_in_button()
    ive_one_main_page.Open_Autotest_project_button()
    ive_one_main_page.Settings_button()
    ive_one_main_page.Features_button()
    ive_one_main_page.DataRoom_switcher()
    time.sleep(3)
    ive_one_main_page.refresh_page()
    ive_one_main_page.Company_tab_Issuer()
    ive_one_main_page.DataRoom_tab_Issuer()
    ive_one_main_page.CreateFolder_button()
    ive_one_main_page.CreateFolder_name_input()
    ive_one_main_page.Create_button()
    ive_one_main_page.DeleteFolder_button()
    ive_one_main_page.FolderDeleted_check()
    ive_one_main_page.Settings_button()
    ive_one_main_page.Features_button()
    ive_one_main_page.DataRoom_switcher()
