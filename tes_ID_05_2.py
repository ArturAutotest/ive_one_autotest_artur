from PageObject import Find_element_click
import time

#_Issuer_Delete_Project

def test_ID5_2_Issuer_Create_Project(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    # ive_one_main_page.log_in_button()

    # ive_one_main_page.email_artur('a.korotkiy@ive.one')

    # ive_one_main_page.password_artur('Qbug.151?200')

    # ive_one_main_page.log_in_button()

    # ive_one_main_page.Project_tab()
    # ive_one_main_page.refresh_page()
    # ive_one_main_page.Project_name_init('Creation Project test')
    # time.sleep(5)
    ive_one_main_page.Open_project_button()
    ive_one_main_page.Settings_button()

    ive_one_main_page.Delete_project_button()

    ive_one_main_page.Delete_Project_input()
    ive_one_main_page.Delete_button()