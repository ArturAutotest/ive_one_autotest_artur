from PageObject import Find_element_click
import time

#_Issuer_Project_Configuration

def test_ID6_Issuer_Project_Configuration(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.email_artur('a.korotkiy@ive.one')
    # ive_one_main_page.password_artur('Qbug.151?200')
    # ive_one_main_page.log_in_button()
    ive_one_main_page.Open_Autotest_project_button()
    ive_one_main_page.Settings_button()
    ive_one_main_page.Project_CONF()

    ive_one_main_page.Short_description_input()
    ive_one_main_page.Link_to_details_input()
    ive_one_main_page.Link_to_invest_input()
    ive_one_main_page.Status_droplist()
    ive_one_main_page.Status_Active_select()
    ive_one_main_page.Visibility_droplist()
    ive_one_main_page.Visibility_Public_select()
    ive_one_main_page.Type_droplist()
    ive_one_main_page.Type_Internal_select()
    ive_one_main_page.next_button()

    ive_one_main_page.Stage_3()
    ive_one_main_page.Project_stage_droplist()
    ive_one_main_page.Project_stage_Setup_select()
    ive_one_main_page.Date_start_input()
    ive_one_main_page.Date_end_input()
    ive_one_main_page.Save_button()
    # ive_one_main_page.popup_Project_saved_successfully_check()

    ive_one_main_page.Security_4()
    ive_one_main_page.Asset_type_droplist()
    ive_one_main_page.Asset_type_Security_token_select()
    ive_one_main_page.Security_type_droplist()
    ive_one_main_page.Security_type_Digital_asset_select()
    ive_one_main_page.Expected_return_5_input()
    ive_one_main_page.next_button()
    # ive_one_main_page.popup_Project_saved_successfully_check()

    ive_one_main_page.Market_5()

    ive_one_main_page.ESG_Score_6()
    ive_one_main_page.Environment_Score5()
    ive_one_main_page.Social_Score7()
    ive_one_main_page.Governance_Score3()
    ive_one_main_page.next_button()
    # ive_one_main_page.popup_Project_saved_successfully_check()

    ive_one_main_page.Issuer_information_7()
    ive_one_main_page.Issuer_input()
    ive_one_main_page.Company_website_input()
    ive_one_main_page.Representative_input()
    # ive_one_main_page.next_button()
    # ive_one_main_page.popup_Project_saved_successfully_check()