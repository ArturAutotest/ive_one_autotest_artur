from PageObject import Find_element_click
import time

#_Issuer_Project_details_CONF

def test_ID9_Issuer_Project_details_CONF(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.email_artur('a.korotkiy@ive.one')
    # ive_one_main_page.password_artur('Qbug.151?200')
    # ive_one_main_page.log_in_button()
    ive_one_main_page.Open_Autotest_project_button()
    ive_one_main_page.Settings_button()

    ive_one_main_page.Project_Details_CONF()
    ive_one_main_page.Create_button()
    ive_one_main_page.Language_droplist()
    ive_one_main_page.Language_English()
    ive_one_main_page.Long_description_input()
    ive_one_main_page.Save_button()
    # ive_one_main_page.popup_Project_details_check()
    ive_one_main_page.Delete_button()
    # ive_one_main_page.popup_Project_details_deleted_check()