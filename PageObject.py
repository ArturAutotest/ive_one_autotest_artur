import pytest
from BaseApp import BasePage
from selenium.webdriver.common.by import By
import time
import allure
import json
import requests
import random
import string
from allure_commons.types import AttachmentType
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

class Waits(BasePage):
    @pytest.mark.parametrize('locator')
    def Explicit_wait(self, locator, time_wait=10):
        time_start = time.time()
        # time_wait = 10     #timeout
        wait = WebDriverWait(self.driver, time_wait)
        for i in range(1, time_wait*4):
                    time.sleep(0.25)
                    time_run= time.time()
                    different_time = time_run - time_start
                    if (different_time <= time_wait) :
                        try :
                            wait.until(EC.element_to_be_clickable((By.XPATH, locator))).click()
                            return
                        except :
                            continue
                    else :
                        with allure.step('Error_Screenshot'):
                            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
                        exit(f"Time runs out, element '{locator}' don't exist at page")

    
    @pytest.mark.parametrize('locator')
    def Explicit_wait_without_click(self, locator, time_wait=10):
        time_start = time.time()
        # time_wait = 10     #timeout
        wait = WebDriverWait(self.driver, time_wait)
        for i in range(1, time_wait*4):
                    time.sleep(0.25)
                    time_run= time.time()
                    different_time = time_run - time_start
                    if (different_time <= time_wait) :
                        try :
                            wait.until(EC.element_to_be_clickable((By.XPATH, locator))).click()
                            return
                        except :
                            continue
                    else :
                        with allure.step('Error_Screenshot'):
                            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
                        exit(f"Time runs out, element '{locator}' don't exist at page")
    # @pytest.mark.parametrize('locator')
    # def Explicit_wait(self, locator):
    #     wait = WebDriverWait(self.driver,10)
    #     succeed = False
    #     while succeed != True:
    #         try:
    #             wait.until(EC.element_to_be_clickable((By.XPATH, locator))).click()
    #             succeed = True
    #         except:
    #             pass



class Find_element_click(BasePage):
    # Custom Waits

    # wait = WebDriverWait(self.driver,10)
    #     succeed = False
    #     while succeed != True:
    #         try:
    #             wait.until(EC.element_to_be_clickable((By.XPATH,"//span[contains(text(), '4')]/.."))).click()
    #             succeed = True
    #         except:
    #             pass

    # @pytest.mark.parametrize('password')

    @pytest.mark.parametrize('locator')
    def Element_click(self, locator):
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.click()

                                                    #Investment HUB

    #Main buttons
    
    
    def confirm_payment_button(self):
        locator = "//span[contains(text(),'Confirm')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_field = self.driver.find_element(By.XPATH, locator)
        # search_field.click()


    def continue_button(self):
        locator = "//span[contains(text(),'Continue')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def company_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(),'Company')]")
        search_box.click()

    @allure.feature('Click on Individual_investor button')
    def individual_investor_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//h3[contains(text(),'Individual investor')]")
        search_field.click()
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)

    @allure.feature('Click on sign in button')
    def sign_in_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(),'Sign in')]")
        search_field.click()
        with allure.step('Делаем скриншот'):
            allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
    
    def ok_popup(self):
        locator = "//span[contains(text(),'OK')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, "//span[contains(text(),'OK')]")
        # search_box.click()
    
    def switch_page_2(self):
        self.driver.implicitly_wait(10)
        self.driver.switch_to.window(self.driver.window_handles[1])
    
    def next_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(),'Next')]/..")
        search_box.click()

    def invest_button(self):
        locator = "//span[text() = 'Invest']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
    
    def confirm_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(),'confirm')]")
        search_box.click()


    #Login
    def log_in_button(self):
        locator = "//span[contains(text(),'Log in')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        time.sleep(1)

    def sign_in_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Sign in')]")
        search_box.click()
    
    @pytest.mark.parametrize('name')
    def email_artur(self, name):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//input[@name='email']")
        search_box.send_keys(name)
    
    @pytest.mark.parametrize('password')
    def password_artur(self, password):
        search_box = self.driver.find_element(By.XPATH, "//input[@name='password']")
        search_box.send_keys(password)

    def waited_func_2(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(),'Professional investor')]")
        search_box.click()
    
    
    #Investing    
    def invest_vogeman_green_ship(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text()='Vogemann Green Ship']/..//../..//..//..//..//span[contains(text(), 'Buy')]")
        search_box.click()

    def invest_Bitcoin_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text()='Bitcoin']/..//../..//..//..//..//span[contains(text(), 'Buy')]")
        search_box.click()

    def buy_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//button//span[contains(text(),'Buy')]")
        search_box.click()

    def purchase_order_confirmations_1(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[@for='checkbox-document-item-0']//div")
        search_box.click()
        # self.driver.implicitly_wait(10)
        # search_box = self.driver.find_element(By.XPATH, "//label[@for='checkbox-document-item-0']//div")
        # search_box.click()
        # self.driver.implicitly_wait(10)
        # search_box = self.driver.find_element(By.XPATH, "//label[@for='checkbox-document-item-1']//div")
        # search_box.click()
        # self.driver.implicitly_wait(10)
        # search_box = self.driver.find_element(By.XPATH, "//label[@for='checkbox-document-item-2']//div")
        # search_box.click()
        # self.driver.implicitly_wait(10)
        # search_box = self.driver.find_element(By.XPATH, "//label[@for='checkbox-document-item-3']//div")
        # search_box.click()
        # self.driver.implicitly_wait(10)
        # search_box = self.driver.find_element(By.XPATH, "//label[@for='checkbox-document-item-4']//div")
        # search_box.click()
    
    #Payment methods
    def payment_methods(self):
        locator = "//p[contains(text(),'Bank transfer')]/../.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, locator)
        # search_box.click()

    # Fiat account
    def fiatAccount_method(self):
        locator = "//p[contains(text(), 'Fiat account')]/../.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    #Paypal
    def paypal_method(self):
        locator = "//p[contains(text(), 'Paypal account')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Paypal_buy_button(self):
        time.sleep(8)
        iframe = "//iframe[@title='PayPal']"
        iframe_elem = self.driver.find_element(By.XPATH, iframe)
        # locator = "//img[@role = 'presentation']//.."
        # button = "//button[@disabled]"
        # button = "//span[contains(text(), 'Buy')]/..//..//button"
        # wait = Waits(self.driver)
        # wait.Explicit_wait_without_click(button)
        # self.driver.switch_to.frame(self.driver.find_element(By.XPATH, iframe))
        # element = self.driver.find_element(By.XPATH, button)
        iframe_elem.click()

    def paypal_buy_Bitcoin(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//div[@id='paypal-button-container-12']")
        search_box.click()

    @pytest.mark.parametrize('email')
    def paypal_email_parametrize(self, email):
        self.driver.implicitly_wait(10)
        locator = "//input[@id='email']"
        search_box = self.driver.find_element(By.XPATH, locator)
        search_box.send_keys(email)

    def paypal_next(self):
        locator = "//button[contains(text(), 'Next')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, "//button[contains(text(), 'Далее')]")
        # search_box.click()

    @pytest.mark.parametrize('password')
    def paypal_password(self, password):
        self.driver.implicitly_wait(10)
        locator = "//input[@id='password']"
        search_box = self.driver.find_element(By.XPATH, locator)
        search_box.send_keys(password)

    def paypal_login(self):
        locator = "//button[contains(text(), 'Log In')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, "//button[contains(text(), 'Войти')]")
        # self.driver.execute_script("arguments[0].click();", search_box)
        #search_box.click()

    def paypal_pay_now(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//button[contains(text(), 'Pay Now')]")
        self.driver.execute_script("arguments[0].scrollIntoView();", search_box)
        self.driver.execute_script("arguments[0].click();", search_box)
        #search_box.click()

    def paypal_X(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(), 'close coach tip')]/..")
        search_box.click()
    
    #Credit Card
    def credit_card_method(self):
        locator = "//p[contains(text(), 'Credit Card')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Credit Card')]")
        # search_box.click()

    def cardholder_name(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Cardholder Name *')]/..//div//input")
        search_box.send_keys("Artur")

    def pay_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//button//span[contains(text(),'Pay')]")
        search_box.click()

    #Checkings
    def successfully_payment_check(self):
        locator = "//p[contains(text(), 'You have successfully completed the pay process!')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        assert element.text == "You have successfully completed the pay process!"

    def successfully_payment_paypal_check(self):
        self.driver.implicitly_wait(10)
        self.driver.switch_to.window(self.driver.window_handles[0])
        congratulations = "//h4[contains(text(), 'Congratulations!')]"
        congratulations2 = "//p[contains(text(), 'You have successfully completed the pay process!')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(congratulations)
        popup = self.driver.find_element(By.XPATH, congratulations)
        assert popup.text == "Congratulations!"
        popup2 = self.driver.find_element(By.XPATH, congratulations2)
        assert popup2.text == "You have successfully completed the pay process!"

    def popup_settings_saved_check(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Sales setting saved successfully')]").text
        assert search_field == "Sales setting saved successfully"

    def popup_Sales_configuration_deleted_check(self):
        locator = "//p[contains(text(), 'Sales configuration deleted successfully')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        elem = self.driver.find_element(By.XPATH, locator)
        assert elem.text == "Sales configuration deleted successfully"

    def popup_Project_details_check(self):
        locator = "//p[contains(text(), 'Project details were created successfully')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        elem = self.driver.find_element(By.XPATH, locator)
        assert elem.text == "Project details were created successfully"

    def popup_Project_details_deleted_check(self):
        locator = "//p[contains(text(), 'Project details deleted successfully')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        search_field = self.driver.find_element(By.XPATH, locator)
        assert search_field.text == "Project details deleted successfully"

    def popup_Project_saved_successfully_check(self):
        locator = "//div//p[contains(text(), 'Project saved successfully')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        assert element.text == "Project saved successfully"

    def popup_Onboarding_was_successfully_updated_check(self):
        self.driver.implicitly_wait(10)
        time.sleep(1)
        search_field = self.driver.find_element(By.XPATH, "//div//p[contains(text(), 'Onboarding was successfully updated')]").text
        assert search_field == "Onboarding was successfully updated"

    def popup_Onboarding_was_successfully_deleted_check(self):
        self.driver.implicitly_wait(10)
        time.sleep(1)
        search_field = self.driver.find_element(By.XPATH, "//div//p[contains(text(), 'Onboarding was successfully deleted')]").text
        assert search_field == "Onboarding was successfully deleted"

    #iframe
    def card_number(self):
        self.driver.implicitly_wait(10)
        self.driver.switch_to.frame(self.driver.find_element(By.XPATH, "//iframe[@title='Secure card number input frame']"))
        creditcardNumber = self.driver.find_element(By.XPATH, "//input[@placeholder='1234 1234 1234 1234']")
        creditcardNumber.send_keys("4242424242424242")
        self.driver.switch_to.default_content()


    def card_month_year(self):
        self.driver.implicitly_wait(10)
        self.driver.switch_to.frame(self.driver.find_element(By.XPATH, "//div//iframe[@title='Secure expiration date input frame']"))
        creditcardmmyy = self.driver.find_element(By.XPATH, "//input[@placeholder='MM / YY']")
        creditcardmmyy.send_keys("1135")
        self.driver.switch_to.default_content()

    def card_cvc(self):
        self.driver.implicitly_wait(10)
        self.driver.switch_to.frame(self.driver.find_element(By.XPATH, "//iframe[@title='Secure CVC input frame']"))
        creditcardCvc = self.driver.find_element(By.XPATH, "//input[@placeholder='CVC']")
        creditcardCvc.send_keys("123")
        self.driver.switch_to.default_content()




                                                    #Issuer portal
    #Create project
    def Project_tab(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(),'Projects')]/..")
        search_box.click()

    def refresh_page(self):
        self.driver.refresh()


    def Create_new_asset(self):
        locator = "//span[contains(text(),'Create new asset')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def New_project_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//div//p[contains(text(),'New Project')]")
        search_box.click()  

    # def Project_Autotest_name_input(self):
    #     self.driver.implicitly_wait(10)
    #     search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Project name *')]//..//input")
    #     search_box.send_keys("Autotest")

    @pytest.mark.parametrize('project_name')
    def Project_name_init(self, project_name):
        self.project_name = project_name
    # project_name = ''
    @pytest.mark.parametrize('project_name')
    def Asset_name_input(self):
        # self.project_name = project_name
        locator = "//label[contains(text(), 'Asset name *')]//..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        search_box = self.driver.find_element(By.XPATH, locator)
        search_box.send_keys(str('Autotest'))
    
    def Asset_symbol_input(self, randomSymbol):
        locator = "//label[contains(text(), 'Project Letters ID *')]//..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        search_box = self.driver.find_element(By.XPATH, locator)
        search_box.send_keys(str(randomSymbol))

    def Internal_Type_selector(self):
        self.driver.implicitly_wait(10)
        typeSelector = self.driver.find_element(By.XPATH, "//div//label[contains(text(), 'Type *')]/..")
        typeSelector.click()
        internalProjectSelect = self.driver.find_element(By.XPATH, "//li//p[contains(text(), 'Internal project')]")
        internalProjectSelect.click()

    def Internal_project_radio_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Internal project')]")
        search_box.click()

    def Create_button(self):
        locator = "//span[text()='Create']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, "//span[text()='Create']/..")
        # search_box.click()


    def Open_Auto1_project_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Auto1 FT')]//..//..//span[contains(text(), 'See more')]")
        search_box.click()

    def Open_project_button(self):
        # {self.project_name}
        self.driver.implicitly_wait(10)
        locator = f"//p[contains(text(), 'Creation Project test')]//..//..//span[contains(text(), 'See more')]"
        # wait = Waits(self.driver)
        # wait.Explicit_wait_without_click(locator)
        elem = self.driver.find_element(By.XPATH, locator)
        elem.click()

    def Open_Autotest_project_button(self):
        locator = "//p[contains(text(), 'Autotest')]//..//..//span[contains(text(), 'See more')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    #Settings(Issuer portal)
    def Settings_button(self):
        locator = "//span[contains(text(), 'Settings')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Delete_asset(self, projectName):
        locator = "//span[contains(text(), 'Delete asset')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

        locator2 = f"//label[contains(text(), '{projectName} *')]//..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator2)
        element2 = self.driver.find_element(By.XPATH, locator2)
        element2.send_keys(projectName)
    
        locator3 = "//span[text()='Delete']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator3)
 

    #CONFIGURATORS
    def Sales_CONF(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Sales')]//..")
        search_box.click()

    def Create_new_configuration_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Create new configuration')]//..")
        search_box.click()

    #Sales CONF - Sale Details
    
    def Retail_investor_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Retail')]/..")
        search_box.click()

    def proffesional_investor_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//p[contains(text(),'Professional')]/..")
        search_field.click()
    
    def Save_button(self):
        wait = WebDriverWait(self.driver,10)
        succeed = False
        while succeed != True:
            try:
                wait.until(EC.element_to_be_clickable((By.XPATH, "//span[contains(text(), 'Save')]/.."))).click()
                succeed = True
            except:
                pass

    def Deploy_Token(self):
        locator = "//span[text() = 'Deploy']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator, 20)

    def Sale_Configuration_2(self):
        locator = "//span[contains(text(), 'Sale Configuration')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        elem = self.driver.find_element(By.XPATH, locator)
        elem.click()


    def Min_invest_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Min Invest *')]//..//input")
        search_box.send_keys("100")
    
    def Max_invest_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Max Invest *')]//..//input")
        search_box.send_keys("1000")

# =====================================================================

    def Payment_Configuration_3(self):
        locator = "//span[contains(text(), 'Payment Configuration')]/../.."
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        elem = self.driver.find_element(By.XPATH, locator)
        elem.click()

    def Add_new_payment_method_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Add new payment method')]/..")
        search_field.click()
    
    def Currency_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Currency *')]//..//div")
        search_box.click()

    def EUR(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'EUR')]/..")
        search_box.click()

    def Payment_Provider_Type_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Payment Provider Type *')]//..//div")
        search_box.click()

    def Bank_transfer(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Bank transfer')]/..")
        search_box.click()

    def Holder_name_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Holder name *')]//..//input")
        search_box.send_keys("Artur")

    def IBAN_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'IBAN *')]//..//input")
        search_box.send_keys("UAYYZZZZZZ0000012345678912345")

    def Bank_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Bank *')]//..//input")
        search_box.send_keys("dsada")

    def BIC_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'BIC *')]//..//input")
        search_box.send_keys("ewqewq2313")

    def Bank_address_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Bank address *')]//..//input")
        search_box.send_keys("ewqeq")
    
    def Country_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Country *')]//..//input")
        search_box.send_keys("Украина")
    
    def Address_owner_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Address owner *')]//..//input")
        search_box.send_keys("ewqeq")

# =====================================================================

    def Fees_Configuration_4(self):
        wait = WebDriverWait(self.driver,10)
        succeed = False
        while succeed != True:
            try:
                wait.until(EC.element_to_be_clickable((By.XPATH,"//span[contains(text(), '4')]/.."))).click()
                succeed = True
            except:
                pass

    def Add_new_fee_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Add new fee')]/..")
        search_field.click()

    def Fixed_fee_name_input(self):
        self.driver.implicitly_wait(10)
        feeNameInput = self.driver.find_element(By.XPATH, "//label[contains(text(), 'name *')]//..//input")
        feeNameInput.send_keys("Fixed Fee")

    def Fixed_fee_description_input(self):
        self.driver.implicitly_wait(10)
        feeDescriptionInput = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Description *')]//..//input")
        feeDescriptionInput.send_keys("Fixed Fee Description")

    def Fee_Type_droplist(self):
        self.driver.implicitly_wait(10)
        feeTypeDroplist = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Type *')]//../div")
        feeTypeDroplist.click()
        feeFixed = self.driver.find_element(By.XPATH, "//li//p[contains(text(), 'Fixed')]")
        feeFixed.click()

    def Fixed_fee_value_input(self):
        self.driver.implicitly_wait(10)
        fixedFeeValue = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Value *')]//..//input")
        fixedFeeValue.send_keys("20")


# =====================================================================

    def File_Configuration_5(self):
        wait = WebDriverWait(self.driver,10)
        succeed = False
        while succeed != True:
            try:
                wait.until(EC.element_to_be_clickable((By.XPATH,"//span[contains(text(), '5')]/.."))).click()
                succeed = True
            except:
                pass

    def Add_new_file_button(self):
        self.driver.implicitly_wait(10)
        addFileButton = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Add new file')]/..")
        addFileButton.click()

    def File_name_input(self):
        self.driver.implicitly_wait(10)
        fileNameInput = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Name of file *')]//..//input")
        fileNameInput.send_keys("File name")

    def File_Language_droplist(self):
        self.driver.implicitly_wait(10)
        fileLanguageDroplist = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Language *')]//../div")
        fileLanguageDroplist.click()
        fileEnglish = self.driver.find_element(By.XPATH, "//li//p[contains(text(), 'English')]")
        fileEnglish.click()

    def Upload_file_button(self):
        self.driver.implicitly_wait(10)
        addFileButton = self.driver.find_element(By.XPATH, "//input[@type='file']")
        addFileButton.send_keys("C:/Users/Артур/Downloads/Arturovich.jpg")

# =====================================================================
    
    def To_sales_settings_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'To sales settings')]")
        search_field.click()
    
    def Delete_Configuration_button(self):
        locator = "//span[contains(text(), 'Delete')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        elem = self.driver.find_element(By.XPATH, locator)
        elem.click()
   
    #Project Details CONF
    def Project_Details_CONF(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Project details')]/..")
        search_box.click()

    def Create_new_project_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Create new project details')]/..")
        search_box.click()

    def Language_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Language *')]/..")
        search_box.click()

    def Language_English(self):
        locator = "//li//p[contains(text(), 'English')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'English')]/..")
        # search_box.click()

    def Long_description_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Long description *')]/..//textarea")
        search_box.send_keys("Automation")

    #Project CONF
    def Project_CONF(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Project']/..")
        search_box.click()

        # 1 PROJECT INFORMATION
    def Name_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'name *')]/..//input")
        search_box.clear()
        search_box.send_keys("Autotest Name")

    def Short_description_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Short description *')]/..//textarea")
        search_box.clear()
        search_box.send_keys("Short description")

    def Link_to_details_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Link to details *')]/..//input")
        search_box.clear()
        search_box.send_keys("https://www.ive.one/en/invest")

    def Link_to_invest_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Link to invest *')]/..//input")
        search_box.clear()
        search_box.send_keys("https://www.ive.one/en/invest")

    def Status_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Status *')]/..")
        search_box.click()

    def Status_Active_select(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Active')]/..")
        search_box.click()

    def Visibility_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Visibility *')]/..")
        search_box.click()

    def Visibility_Public_select(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Public')]/..")
        search_box.click()
    
    def Type_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Type *')]/..")
        search_box.click()

    def Type_Internal_select(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Internal')]/..")
        search_box.click()
    #2
    def Graphic_Files_2(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Graphic Files')]//..//span[contains(text(), '2')]")
        search_field.click()
    #3
    def Stage_3(self):
        # self.driver.implicitly_wait(10)
        locator = "//span[contains(text(), 'Stage')]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        # search_field = self.driver.find_element(By.XPATH, locator)
        # search_field.click()

    def Project_stage_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Project stage *')]/..")
        search_box.click()

    def Project_stage_Setup_select(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Setup')]/..")
        search_box.click()

    def Date_start_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Date of Start *')]/..//input")
        search_box.clear()
        search_box.send_keys("07/07/2022")

    def Date_end_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Date of End *')]/..//input")
        search_box.clear()
        search_box.send_keys("09/09/2024")
      
    #4
    def Security_4(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Security')]/..")
        search_field.click()

    def Asset_type_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Asset type *')]/..")
        search_box.click()

    def Asset_type_Security_token_select(self):
        locator = "//p[contains(text(), 'Security token')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Security_type_droplist(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Security type *')]/..")
        search_box.click()

    def Security_type_Digital_asset_select(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Digital asset')]/..")
        search_box.click() 

    def Expected_return_5_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Expected return *')]/..//input")
        search_box.clear()
        search_box.send_keys("5")

    #5
    def Market_5(self):
        locator = "//span[contains(text(), 'Market')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.click()    

    #6
    def ESG_Score_6(self):
        locator = "//span[contains(text(), 'ESG score')]/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.click()

    def Environment_Score5(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Environment Score *')]/..//input")
        search_box.clear()
        search_box.send_keys("5")

    def Social_Score7(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Social Score *')]/..//input")
        search_box.clear()
        search_box.send_keys("7")
    
    def Governance_Score3(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Governance Score *')]/..//input")
        search_box.clear()
        search_box.send_keys("3")

    #7
    def Issuer_information_7(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Issuer Information')]/..")
        search_field.click()

    def Issuer_input(self):
        locator = "//label[contains(text(), 'Issuer')]/..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        search_box = self.driver.find_element(By.XPATH, locator)
        search_box.click()
        search_box.clear()
        search_box.send_keys("Artur Korotkiy")
        # search_box.send_keys(Keys.ENTER)

    def Company_website_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Company website *')]/..//input")
        search_box.clear()
        search_box.send_keys("https://youtube.com/")

    def Representative_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'Representative *')]/..//input")
        search_box.clear()
        search_box.send_keys("Representative")

    def To_settings_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'To settings')]")
        search_field.click()

    # Project Settings
    def Project_Settings(self):
        locator = "(//span[text() = 'Settings'])[2]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    # Token config
    def Token_Config(self):
        locator = "//p[text() = 'Token']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
    
    def TokenAmount_input(self, amount):
        locator = "//label[text() = 'Amount of token *']//..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.clear()
        element.send_keys(amount)

    def TokenNetwork(self):
        locator = "(//label[text() = 'Blockchain network *']//..)[1]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        locator2 = "//p[text() = 'Sepolia testnet']"
        element = self.driver.find_element(By.XPATH, locator2)
        element.click()

    def Token_NameOfSmartContract(self):
        locator = "(//label[text() = 'Name of smart contract *']//..)[1]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        locator2 = "//p[text() = 'Sepolia SM1']"
        element = self.driver.find_element(By.XPATH, locator2)
        element.click()

    def LinkToSmartContract(self):
        locator = "//p[text() = 'Link of smart contract']//..//a[@href!='']"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator, 120)

    #Onboarding CONF
    def Onboarding_CONF(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Onboarding']/..")
        search_box.click()

    #1 Investor details
    def Investor_button(self):
        locator = "//p[text() = 'Investor']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Employee_Invesor_details_button(self):
        locator = "//p[text() = 'Employee']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Advisor_Invesor_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Advisor']/..")
        search_box.click()

    def Retail_Invesor_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Retail']/..")
        search_box.click()

    def Professional_Invesor_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Professional']/..")
        search_box.click()

    def Private_person_Invesor_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Private person']/..")
        search_box.click()

    def Company_Invesor_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Company']/..")
        search_box.click()
    
    #2 ID Verification
    def Turn_on_button(self):
        self.driver.implicitly_wait(10)
        time.sleep(1)
        search_box = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Turn On')]")
        search_box.click()
        search_box.click()


    def ID_Verification_Provider_button(self):
        self.driver.implicitly_wait(10)
        time.sleep(1)
        search_box = self.driver.find_element(By.XPATH, "//label[text() = 'ID Verification Provider']/..")
        search_box.click()

    def Postident_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Postident']/..")
        search_box.click()
    
    #4 Onboarding Steps
    #Company
    def Company_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Company details']/../..//label[@for='checkbox-company-details']")
        search_box.click()

    def Personal_details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Personal Details']/../..//label[@for='checkbox-company-personal-details']")
        search_box.click()   

    def Knowledge_and_experience_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Knowledge and experience']/../..//label[@for='checkbox-company-knowledge']")
        search_box.click() 
    
    def Stakeholders_section_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Stakeholders section']/../..//label[@for='checkbox-company-stakeholders']")
        search_box.click() 
    
    #Personal
    def Personal_Personal_Details_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Personal Details']/../..//label[@for='checkbox-personal-details']")
        search_box.click()

    def Legal_Issues_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Legal Issues']/../..//label[@for='checkbox-personal-legal-issues']")
        search_box.click()

    def Knowledge_and_experience_button(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//p[text() = 'Knowledge and experience']/../..//label[@for='checkbox-personal-knowledge-experience']")
        search_box.click()

    def To_onboarding_list_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'To onboarding list')]")
        search_field.click()

    #Settings Features
    def Features_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Features')]")
        search_field.click()
    
    def DataRoom_switcher(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Data Room')]/..//input")
        search_field.click()

    def DataRoom_switchON_check(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//p[contains(text(), 'Enable feature success')]").text
        assert search_field == "Enable feature success"
 
    def Company_tab_Issuer(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Company')]/..")
        search_field.click()

    def CapTable_tab(self):
        locator = "//span[text() = 'Cap Table']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Investors_subtab(self):
        locator = "(//span[text() = 'Investors'])[2]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def check_ShareholderAmount(self):
        # locator = "(//tr)[2]//td[1]"
        # wait = Waits(self.driver)
        # wait.Explicit_wait_without_click(locator)
        # element = self.driver.find_element(By.XPATH, locator)
        # assert element.text == 'Artur Korotkiy'
        locator1 = "(//tr)[2]//td[2]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator1)
        element1 = self.driver.find_element(By.XPATH, locator1)
        assert element1.text == '555000'
        locator2 = "(//tr)[2]//td[4]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator2)
        element2 = self.driver.find_element(By.XPATH, locator2)
        assert element2.text == '100 %'

    def add_Investor(self):
        locator = "//span[text() = 'Add Investor']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def email_field(self, email):
        locator = "//label[text() = 'Email *']/..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.send_keys(email)
    
    def firstName_field(self, name):
        locator = "//label[text() = 'First name *']/..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.send_keys(name)

    def lastName_field(self, lastName):
        locator = "//label[text() = 'Last name *']/..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        element.send_keys(lastName)

    def add_button(self):
        locator = "//span[text() = 'Add']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Trading_tab(self):
        locator = "//span[text() = 'Trading']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
    
    def Orders_tab(self):
        locator = "//span[text() = 'Orders']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def check_InvestorOrder(self):
        locator = "(//tr)[2]//td[5][contains(text(), '1')]"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        element = self.driver.find_element(By.XPATH, locator)
        assert '1' in element.text
        locator2 = "(//tr)[2]//td[6]"
        element2 = self.driver.find_element(By.XPATH, locator2)
        assert 'Executed' in element2.text
    

    def enablePrimaryMarket(self):
        locator = "//span[text() = 'Enable Primary Market']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def createSellOrder(self):
        # Createting sell order on Issuer portal
        '''Click to Create Sell order button'''
        locator = "//span[text() = 'Create Sell Order']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

        '''Fill initial amount input'''
        locator2 = "//label[contains(text(), 'Initial amount')]/..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator2)
        element = self.driver.find_element(By.XPATH, locator2)
        element.send_keys('10')

        '''Fill Price input'''
        locator3 = "//label[contains(text(), 'Price *')]/..//input"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator3)
        element = self.driver.find_element(By.XPATH, locator3)
        element.send_keys('1')

        '''Click to Create button'''
        locator4 = "//span[text() = 'Create']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator4)


    def logout(self):
        locator = "//p[text() = 'English']/../..//button"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)
        locator2 = "//span[text() = 'Log out']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator2)

    def Portfolio_tab(self):
        locator = "//span[text() = 'Portfolio']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Invest_to_Autotest(self):
        locator = "//span[text() = 'Autotest']"
        wait = Waits(self.driver)
        wait.Explicit_wait_without_click(locator)
        locator = "//span[text() = 'Invest']/.."
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def transfers_tab(self):
        locator = "//span[text() = 'Transfers']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def DataRoom_tab_Issuer(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Data Room')]")
        search_field.click()

    def CreateFolder_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Create folder')]")
        search_field.click()
    
    def CreateFolder_name_input(self):
        self.driver.implicitly_wait(10)
        search_box = self.driver.find_element(By.XPATH, "//label[contains(text(), 'name')]/..//input")
        search_box.send_keys("Folder")

    def DeleteFolder_button(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//button[@class = 'MuiButtonBase-root MuiIconButton-root sc-bYoBSM dJHWxT']")
        search_field.click()
        search_field = self.driver.find_element(By.XPATH, "//span[contains(text(), 'Delete')]")
        search_field.click()

    def FolderDeleted_check(self):
        self.driver.implicitly_wait(10)
        search_field = self.driver.find_element(By.XPATH, "//h4[contains(text(), 'This folder is empty')]").text
        assert search_field == "This folder is empty"

    def open_tabMenu(self):
        locator = "(//button)[1]"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def Assets_tab(self):
        locator = "//span[text() = 'Assets']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

    def RealEstate_button(self):
        locator = "//p[text() = 'Real Estate']"
        wait = Waits(self.driver)
        wait.Explicit_wait(locator)

#------------------------------------------------------------------------------------------------API------------------------------------------------------------------------------------------------------------------
class RandomEmailGenerator():
    def __init__(self) -> None:
        self.email = ''

    def RandomEmail(self):
        validchars='abcdefghijklmnopqrstuvwxyz1234567890'
        loginlen=random.randint(4,15)
        login=''
        for i in range(loginlen):
            pos=random.randint(0,len(validchars)-1)
            login=login+validchars[pos]
        if login[0].isnumeric():
            pos=random.randint(0,len(validchars)-10)
            login=validchars[pos]+login
        servers=['@gmail','@yahoo','@redmail','@hotmail','@bing']
        servpos=random.randint(0,len(servers)-1)
        email=login+servers[servpos]
        tlds=['.com','.in','.gov','.ac.in','.net','.org']
        tldpos=random.randint(0,len(tlds)-1)
        email=email+tlds[tldpos]
        self.email = email

class WorkWithJSON():
    def __init__(self):
        self.test_data = ''
        self.result = ''

    def writeParameterTo_JSON(self, filename, parameterName, parameterValue):
            with open(f'{filename}.json', 'r') as file:
                self.test_data = json.load(file)
                self.test_data[f'{parameterName}'] = parameterValue
            with open(f'{filename}.json', 'w') as file:
                json.dump(self.test_data, file, indent=4)

    # def addParameterTo_JSON(self, filename, parameterList, response):
    #     for i in parameterList:
    #         parameter = i
    #         parameterName = parameter
    #         result = response.json()
    #         parameterValue = result[f"{parameter}"]
    #         with open(f'{filename}.json', 'r') as file:
    #             self.test_data = json.load(file)
    #             self.test_data[f'{parameterName}'] = parameterValue
    #         with open(f'{filename}.json', 'w') as file:
    #             json.dump(self.test_data, file, indent=4)

    def edit_JSON(self, filename, parameterName, parameterList):
            parameterName = parameterName
            parameterValue = parameterList
            
            for i in parameterList:
                parameter = i
                parameterValue = parameter

            with open(f'{filename}.json', 'r') as file:
                self.test_data = json.load(file)
                self.test_data[f'{parameterName}'] = parameterValue
            with open(f'{filename}.json', 'w') as file:
                json.dump(self.test_data, file, indent=4)

                # Example:
                # dictionary = {
                #     "wow" : "owo"
                # }
                # apiTest.edit_JSON('wow', dictionary)


class API_tests():
    def __init__(self):
        with open('test_data.json') as file:
            self.test_data = json.load(file)
        with open('user_data.json') as file:
            self.user_data = json.load(file)
        # self.idToken = self.test_data['idToken']
        self.result = ''
        self.symbol = ''
        self.idToken = ''
        self.result_JSON = {}
        self.headers = {
        "accept": "*/*",
        "content-type": "application/json",
        "Authorization": f"Bearer {self.test_data['idToken']}"
    }
        # self.headers = self.test_data['headers']
        self.domain_gateway = 'https://gateway-auth.test.ive.one'
        self.domain_investment = 'https://investment-back.test.ive.one'
        self.domain_issuer = 'https://issuer-back.test.ive.one'


    def waiter(self, url, timer, parameter, expectedValue):
        for i in range (timer):
            time.sleep(1)
            self.result = requests.get(url, headers=self.headers)
            if self.result.text != '':
                self.result_JSON = self.result.json()
                # if self.result_JSON[f'{parameter}'] != parameterValue:
                result = eval(parameter)
                if result != expectedValue:
                    print(i)
                    continue
                else:
                    break


        



    def POST_request(self, url, header, data):
        self.result = requests.post(url, headers=header, json=data)
        print(self.result)
        print(self.result.text)
        print(data)
        assert self.result.text != []
        assert self.result.status_code == 200
        if self.result.text != '':
            self.result_JSON = self.result.json()


    def PUT_request(self, url, header, data):
        self.result = requests.put(url, headers=header, json=data)
        print(self.result)
        print(self.result.text)
        print(data)
        assert self.result.text != []
        assert self.result.status_code == 200
        if self.result.text != '':
            self.result_JSON = self.result.json()

    def GET_request(self, url):
        self.result = requests.get(url, headers=self.headers)
        print(self.result)
        print(self.result.text)
        print(self.headers)
        print(self.idToken)
        assert self.result.text != []
        assert self.result.status_code == 200
        if self.result.text != '':
            self.result_JSON = self.result.json()

    def renderRandomSymbol(self):
        letters = string.ascii_uppercase
        self.symbol = (''.join(random.choice(letters)for i in range(random.randrange(3, 7))))

    def addSymbolTo_JSON(self):
        copyProjectInfo = {
                "name": 'Test'+self.symbol,
                "symbol": self.symbol,
                "projectId": 490
                }
        with open('test_data.json', 'r+') as file:
            self.test_data = json.load(file)
            self.test_data['copyProjectInfo'] = copyProjectInfo
            file.seek(0)
            json.dump(self.test_data, file, indent=4)

    def writeParameterTo_JSON(self, paraneterName, *parameterList):
        for i in parameterList:
            parameterValue = i
            edit = WorkWithJSON()
            edit.writeParameterTo_JSON('test_data', paraneterName, parameterValue)

    def edit_JSON(self, parameterName, *parameterValue):
        parameterName = parameterName
        parameterValue = parameterValue
        edit = WorkWithJSON()
        edit.edit_JSON('test_data', parameterName, parameterValue)

    def addTokenInfoTo_JSON(self):
        tokenInfo = {
            "tokenName": self.symbol,
            "tokenSymbol": self.symbol,
            "amount": "800000",
            "network": {
                "id": "6"
            },
            "smartContract": {
                "id": "6"
            },
            "burnable": 'true',
            "mintable": 'true',
            "custodian": "IVEONE"
        }
        with open('test_data.json', 'r+') as file:
            self.test_data = json.load(file)
            self.test_data['tokenInfo'] = tokenInfo
            file.seek(0)
            json.dump(self.test_data, file, indent=4)

    def idTokenUpdate(self, idToken):
        self.headers = {
        "accept": "*/*",
        "content-type": "application/json",
        'Authorization': "Bearer " + idToken
        }

    def addSellOrderInfoTo_JSON(self):
        sellOrderInfo = {
            "projectId": self.test_data["Project_id"],
            "initialAmount": 10,
            "price": 2
        }
        with open('test_data.json', 'r') as file:
            self.test_data = json.load(file)
            self.test_data['sellOrderInfo'] = sellOrderInfo
        with open('test_data.json', 'w') as file:
            json.dump(self.test_data, file, indent=4)

    def add_token_test_data_JSON(self):
        response_json = self.result.json()
        idToken = response_json["idToken"]
        with open('test_data.json', 'r+') as file:
            self.test_data = json.load(file)
            self.test_data['idToken'] = idToken
            file.seek(0)
            json.dump(self.test_data, file, indent=4)

    def check_order_amount(self):
        """fee is 9%"""
        response_json = self.result.json()
        assert response_json[0]['amount'] == (self.user_data["buy_request_body"]["amount"]) + (self.user_data["buy_request_body"]["amount"]*0.09)

    def checkResponse_Parameter(self, parameter, expectedValue):
        if self.result.text != '':
            assert parameter == expectedValue
            # print(parameter)