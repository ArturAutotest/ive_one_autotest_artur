import requests
import json
from PageObject import API_tests

def test_7_PostBuyVogeman_Investment():
    apiTest = API_tests()
    apiTest.POST_request('https://investment-back.test.ive.one/orders/buy', apiTest.headers, apiTest.user_data["buy_request_body"])

def test_8_GetBuyOrder_Investment():
    apiTest = API_tests()
    apiTest.GET_request('https://investment-back.test.ive.one/orders')
    apiTest.check_order_amount()

# with open('test_data.json') as file:
#     token_json = json.load(file)
#     idToken = token_json['idToken']


# url = "https://investment-back.test.ive.one/orders/buy"

# headers = {
#     "accept": "*/*",
#     "content-type": "application/json",
#     'Authorization': "Bearer " + idToken
# }

# buy_request_body = {
#     "projectId": "1",
#     "amount": 2,
#     "type": "BUY",
#     "paymentType": "BANK_TRANSFER"
# }

# result = requests.post(url, headers=headers, json=buy_request_body)
# # response_json = result.json()
# print(result.text)
# print(result)
# # assert result.status_code == 200


