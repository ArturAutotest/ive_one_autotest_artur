from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://investment.test.ive.one/"
        self.issuer_test = "https://issuer.test.ive.one/"

    def go_to_site(self):
        self.driver.implicitly_wait(20)
        return self.driver.get(self.base_url)

    def go_to_issuer(self):
        self.driver.implicitly_wait(10)
        return self.driver.get(self.issuer_test)

    def quit(self):
        self.driver.quit()