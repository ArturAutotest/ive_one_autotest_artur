from PageObject import Find_element_click
import time

#_buy_Vogeman_CreditCard

def test_ID4_buy_Vogeman_CreditCard(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_site()
    # ive_one_main_page.sign_in_button()
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.email_artur('a.korotkiy@ive.one')
    # ive_one_main_page.password_artur('Qbug.151?200')
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.ok_popup()
    ive_one_main_page.invest_vogeman_green_ship()
    ive_one_main_page.payment_methods()
    ive_one_main_page.credit_card_method()
    ive_one_main_page.next_button()
    ive_one_main_page.purchase_order_confirmations_1()
    ive_one_main_page.next_button()
    ive_one_main_page.next_button()
    ive_one_main_page.cardholder_name()
    ive_one_main_page.card_number()
    ive_one_main_page.card_month_year()
    ive_one_main_page.card_cvc()
    ive_one_main_page.pay_button()
    ive_one_main_page.successfully_payment_check()