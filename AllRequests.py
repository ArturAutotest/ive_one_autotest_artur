from PageObject import API_tests, RandomEmailGenerator
import time
import requests


# def test_1_SignUp():
#     apiTest.POST_request(f'{apiTest.domain_gateway}/auth/signUp', apiTest.user_data["signUp_header"], apiTest.user_data["newUserData"])
apiTest = API_tests()
def test_1_Login():
    
    # Login as a.korotkiy@ive.one and update 'idToken' field in test_data.json
    apiTest.POST_request(f'{apiTest.domain_gateway}/auth/login', apiTest.headers, apiTest.test_data["emailPassword"])
    apiTest.idTokenUpdate(apiTest.result_JSON['idToken'])
    # apiTest.headersIdTokenUpdate(apiTest.result_JSON['idToken'])
    # apiTest.headers = {
    #     "accept": "*/*",
    #     "content-type": "application/json",
    #     'Authorization': "Bearer " + apiTest.idToken
    #     }
    # apiTest.writeParameterTo_JSON('headers', apiTest.headers)
    apiTest.writeParameterTo_JSON('idToken', apiTest.result_JSON['idToken'])   

def test_2_GetAllProjects_Investment():
    apiTest.GET_request(f'{apiTest.domain_investment}/external/project/type/IVE_ONE')

def test_3_GetOrders_Investment():
    apiTest.GET_request(f'{apiTest.domain_investment}/orders')

def test_4_GetPerson_Investment():
    apiTest.GET_request(f'{apiTest.domain_investment}/person')

def test_5_GetDashboard_Investment():
    apiTest.GET_request(f'{apiTest.domain_investment}/asset/total')

def test_6_PostBuyVogeman_Investment():
    apiTest.POST_request(f'{apiTest.domain_investment}/orders/buy', apiTest.headers, apiTest.user_data["buy_request_body"])

def test_7_GetBuyOrder_Investment():
    apiTest.GET_request(f'{apiTest.domain_investment}/orders')
    apiTest.check_order_amount()

def test_8_PostOnboarding():
    apiTest.POST_request(f'{apiTest.domain_investment}/person', apiTest.headers, apiTest.user_data["person"])

def test_9_GetAllProjects_Issuer():
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/all')

def test_10_PostOnboardingCONF():
    apiTest.POST_request(f'{apiTest.domain_issuer}/onboarding/config', apiTest.headers, apiTest.user_data["onboardingCONF"])

def test_11_CopyProject():
    # Generate symbol for project and create new project by copying from 490 template project. After project creation update 'Project_id' and 'token' fields in test_data.json
    apiTest = API_tests()
    apiTest.renderRandomSymbol()
    copyProjectInfo = {
                "name": 'Test'+apiTest.symbol,
                "symbol": apiTest.symbol,
                "projectId": apiTest.test_data['copyProjectInfo']['projectId']
                }
    apiTest.edit_JSON('copyProjectInfo', copyProjectInfo)
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_issuer}/project/copy', apiTest.headers, apiTest.test_data["copyProjectInfo"])
    apiTest.writeParameterTo_JSON('Project_id', apiTest.result_JSON["id"])
    apiTest.writeParameterTo_JSON('tokenId', apiTest.result_JSON["token"]['id'])

def test_12_GetProject_Issuer():
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}')

def test_13_GetOnboardingCONF_Issuer():
    apiTest.GET_request(f'{apiTest.domain_issuer}/onboarding/config/project/{apiTest.test_data["Project_id"]}')

def test_14_GetFeatureCONF_Issuer():
    apiTest.GET_request(f'{apiTest.domain_issuer}/feature')

def test_15_GetProjectDetailsCONF_Issuer():
    apiTest.GET_request(f'{apiTest.domain_issuer}/project-details/project/{apiTest.test_data["Project_id"]}')

def test_16_GetSalesCONF_Issuer():
    apiTest.GET_request(f'{apiTest.domain_issuer}/sales/config/project/{apiTest.test_data["Project_id"]}')


def test_17_UpdateToken():
    # Update tokenInfo in test_data.json and update token on project(before deployment)
    apiTest = API_tests()
    tokenInfo = {
        "tokenName": apiTest.test_data['copyProjectInfo']['symbol'],
        "tokenSymbol": apiTest.test_data['copyProjectInfo']['symbol'],
        "amount": apiTest.test_data['tokenInfo']['amount'],
        "network": {
            "id": apiTest.test_data['tokenInfo']['network']['id']
        },
        "smartContract": {
            "id": apiTest.test_data['tokenInfo']['smartContract']['id']
        },
        "burnable": 'true',
        "mintable": 'true',
        "custodian": "IVEONE"
    }
    apiTest.edit_JSON('tokenInfo', tokenInfo)
    apiTest = API_tests()
    apiTest.PUT_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}/token', apiTest.headers, apiTest.test_data["tokenInfo"])

def test_18_DeployToken():
    # Deploy token of project
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_issuer}/tokens/{apiTest.test_data["tokenId"]}/deploy', apiTest.headers, data = '')
    apiTest.GET_request(f'{apiTest.domain_issuer}/smart-contracts/token/{apiTest.test_data["tokenId"]}')
    # Check project/token/address
    url = f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}'
    for i in range (6):
            time.sleep(1)
            apiTest.result = requests.get(url, headers=apiTest.headers)
            if apiTest.result.text != '':
                apiTest.result_JSON = apiTest.result.json()
                # if self.result_JSON[f'{parameter}'] != parameterValue:
                if apiTest.result_JSON['project']['token']['address'] == None:
                    print(i)
                    continue
                else:
                    apiTest.writeParameterTo_JSON('adressToken', apiTest.result_JSON['project']['token']['address'])
                    break

def test_19_GET_Shareholders():
    # Check that Issuer assigned to project
    apiTest = API_tests()
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}')
    apiTest.checkResponse_Parameter(apiTest.result_JSON['shareHolders'][0]["id"], apiTest.test_data['mainShareholderId'])

def test_20_TransferTokensToInvestorA():
    # Modify transferObject in test_data.json
    apiTest = API_tests()
    transfer = {
        "projectId": apiTest.test_data["Project_id"],
        "amount": apiTest.test_data['transfer']['amount'],
        "userEmailTo": apiTest.test_data['InvestorA_email']
        }
    apiTest.edit_JSON('transfer', transfer)
    #Send Transfer and Update 'InvestorA_totalAmount' in test_data.json
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_issuer}/transfers/project', apiTest.headers, apiTest.test_data["transfer"])
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}')
    apiTest.writeParameterTo_JSON('InvestorA_totalAmount', apiTest.result_JSON['shareHolders'][1]['account']['available'])
    # Check 'available' amount
    # apiTest = API_tests()
    
    # apiTest.waiter(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}', 300, "self.result_JSON['shareHolders'][1]['account']['available']", apiTest.test_data['InvestorA_totalAmount'] + transfer['amount'])
    
    # apiTest.checkResponse_Parameter(apiTest.result_JSON['shareHolders'][1]['account']['available'], apiTest.test_data['InvestorA_totalAmount'] + transfer['amount'])
    apiTest.writeParameterTo_JSON('InvestorA_totalAmount', apiTest.result_JSON['shareHolders'][1]['account']['available'])
    
def test_21_sendEuroToInvestorB():
    # Transfer 1 Euro to investorB
    apiTest = API_tests()
    transfer = {
    "projectId": apiTest.test_data['euroProject_Id'],
    "amount": apiTest.test_data['transfer']['amount'],
    "userEmailTo": apiTest.test_data['investorB_email']
    }
    apiTest.POST_request(f'{apiTest.domain_issuer}/transfers/project', apiTest.headers, transfer)

def test_22_InvestorA_CreateSellOrder():
    # Login as InvestorA
    apiTest = API_tests()
    Investor_emailPassword = {
        "email": apiTest.test_data['InvestorA_email'],
        "password": apiTest.test_data['investorPassword']
    }
    apiTest.POST_request(f'{apiTest.domain_gateway}/auth/login', apiTest.headers, Investor_emailPassword)
    apiTest.writeParameterTo_JSON('idToken', apiTest.result_JSON['idToken'])
    # Create sellOrder
    apiTest = API_tests()
    sellOrderBody = {
    "projectId": apiTest.test_data["Project_id"],
    "amount": apiTest.test_data['sellOrderBody_amount_price']['amount'],
    "type": "SELL",
    "paymentType": "ACCOUNT",
    "price": apiTest.test_data['sellOrderBody_amount_price']['price']
    }
    apiTest.POST_request(f'{apiTest.domain_investment}/orders/sell', apiTest.headers, sellOrderBody)
    # Check 'amount', 'type' and 'price'
    apiTest.checkResponse_Parameter(apiTest.result_JSON['amount'], sellOrderBody['amount'])
    apiTest.checkResponse_Parameter(apiTest.result_JSON['type'], sellOrderBody['type'])
    apiTest.checkResponse_Parameter(apiTest.result_JSON['price'], sellOrderBody['price'])

def test_23_InvestorB_buyTokensFromInvestorA():
    # Login as InvestorB
    apiTest = API_tests()
    Investor_emailPassword = {
        "email": apiTest.test_data['investorB_email'],
        "password": apiTest.test_data['investorPassword']
    }
    buy_request_body = {
        "projectId": apiTest.test_data["Project_id"],
        "amount": apiTest.test_data["buy_request_body_amount"],
        "type": "BUY",
        "paymentType": "ACCOUNT"
    }
    apiTest.POST_request(f'{apiTest.domain_gateway}/auth/login', apiTest.headers, Investor_emailPassword)
    apiTest.writeParameterTo_JSON('idToken', apiTest.result_JSON['idToken'])
    # Buy tokens, check 'amount' and 'status' fields
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_investment}/orders/buy', apiTest.headers, buy_request_body)
    apiTest.writeParameterTo_JSON('buyOrderId', apiTest.result_JSON['id'])
    apiTest.GET_request(f'{apiTest.domain_investment}/orders')
    order = apiTest.result_JSON
    filteredById = list(filter(lambda x: x['id'] == apiTest.test_data['buyOrderId'], order))
    filteredOrder = dict(*filteredById)
    apiTest.checkResponse_Parameter(filteredOrder['id'], apiTest.test_data["buyOrderId"])
    apiTest.writeParameterTo_JSON('filteredOrder', filteredOrder)
    # apiTest.checkResponse_Parameter(apiTest.result_JSON[0]['status'], 'PAID')
    # apiTest.checkResponse_Parameter(apiTest.result_JSON[0]['amount'], apiTest.test_data["buy_request_body_amount"])
    # apiTest.checkResponse_Parameter(apiTest.result_JSON[0]['status'], 'PAID')

def test_24_IssuerCreateSellOrder():
    # Login as issuer a.korotkiy@ive.one
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_gateway}/auth/login', apiTest.headers, apiTest.test_data["emailPassword"])
    apiTest.writeParameterTo_JSON('idToken', apiTest.result_JSON['idToken'])
    apiTest.addSellOrderInfoTo_JSON()
    # Create sellOrder and saving it in test_data.json
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_issuer}/sell/order', apiTest.headers, apiTest.test_data["sellOrderInfo"])
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}')
    apiTest.writeParameterTo_JSON('sellOrderProject', apiTest.result_JSON["project"]["sellOrders"][0]["project"]["issuer"])

def test_25_AddInvestor():
    # Create random email for investor
    email = RandomEmailGenerator()
    email.RandomEmail()
    apiTest = API_tests()
    AddInvestor = {
        "email": email.email,
        "firstName": "RandomEmail",
        "lastName": "Test",
        "projectId": apiTest.test_data["Project_id"]
    }
    apiTest.edit_JSON('AddInvestor', AddInvestor)
    # Add this investor to project and check him on project
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_issuer}/user/investor', apiTest.headers, apiTest.test_data["AddInvestor"])
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}')
    apiTest.checkResponse_Parameter(apiTest.result_JSON['investors'][-1]['email'], email.email)

def test_26_TransferTokens():
    # Update 'transfer' info in test_data.json
    apiTest = API_tests()
    transfer = {
        "projectId": apiTest.test_data["Project_id"],
        "amount": apiTest.test_data['transfer']["amount"],
        "userEmailTo": apiTest.test_data["AddInvestor"]["email"]
        }
    apiTest.edit_JSON('transfer', transfer)
    # Transfer tokens to RandomEmailInvestor
    apiTest = API_tests()
    apiTest.POST_request(f'{apiTest.domain_issuer}/transfers/project', apiTest.headers, apiTest.test_data["transfer"])
    # GET project info and save to test_data.json 'RandomEmailInvestorBalance'
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}')
    apiTest.writeParameterTo_JSON('RandomInvestorBalance', apiTest.result_JSON['shareHolders'][-1]['account']['available'])
    # Check if 'RandomEmailInvestorBalance' updated
    apiTest = API_tests()
    # apiTest.waiter(f'{apiTest.domain_issuer}/project/{apiTest.test_data["Project_id"]}', 300, "self.result_JSON['shareHolders'][-1]['account']['available']", apiTest.test_data['RandomInvestorBalance'] + transfer['amount'])
    