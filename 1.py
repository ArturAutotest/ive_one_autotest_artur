

# Login
# POST_request(https://gateway-auth.test.ive.one/auth/login)

# CopyProject
# POST_request(https://issuer-back.test.ive.one/project/copy)

# GetProject_Issuer
# GET_request(https://issuer-back.test.ive.one/project/{Project_id})
            
# UpdateToken
# PUT_request(https://issuer-back.test.ive.one/project/{Project_id}/token)
            
# DeployToken
# POST_request(https://issuer-back.test.ive.one/tokens/{tokenId}/deploy)

# TransferTokensToInvestorA
# POST_request(https://issuer-back.test.ive.one/transfers/project)

# sendEuroToInvestorB             
# POST_request(https://issuer-back.test.ive.one/transfers/project)

# InvestorA_CreateSellOrder
# POST_request(https://investment-back.test.ive.one/orders/sell)

# InvestorB_buyTokensFromInvestorA
# POST_request(https://investment-back.test.ive.one/orders/buy)

# IssuerCreateSellOrder
# POST_request(https://issuer-back.test.ive.one/sell/order)

# AddInvestor
# POST_request(https://issuer-back.test.ive.one/user/investor)
             
# TransferTokens
# POST_request(https://issuer-back.test.ive.one/transfers/project)

