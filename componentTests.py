from PageObject import API_tests
import time
import requests

                                                                        # Issuer portal

apiTest = API_tests()
def test_Login():
    # Sending email and password to login
    apiTest.POST_request(f'{apiTest.domain_gateway}/auth/login', apiTest.headers, apiTest.test_data["emailPassword"])
    apiTest.idTokenUpdate(apiTest.result_JSON['idToken'])
    # Update idToken in test_data.json
    apiTest.writeParameterTo_JSON('idToken', apiTest.result_JSON['idToken']) 

def test_IssuerDashboard():
    # Get all user projects
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/all')
    # Get user info
    apiTest.GET_request(f'{apiTest.domain_issuer}/user')

def test_GetTemplates():
    # Get template projects
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/templates')

def test_Project():
    # Get all project info
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/{apiTest.test_data["componentTestProjectId"]}')
    # Invite Investor
    apiTest.GET_request(f'{apiTest.domain_issuer}/project/invite/investor/{apiTest.test_data["componentTestProjectId"]}/test@artur.mail/ADVISOR')
    # # Add team member to project
    # apiTest.POST_request(f'{apiTest.domain_issuer}/project/invite/{apiTest.test_data["componentTestProjectId"]}/test@artur.mail/ISSUER')



def test_Issuer_InvestorsList():
    # Get list of Investors connected to whitelabel
    apiTest.GET_request(f'{apiTest.domain_issuer}/portfolio/investors/IVE_ONE/search?pageSize=2000')
    # Add Investor
    apiTest.POST_request(f'{apiTest.domain_issuer}/user/investor', apiTest.headers, apiTest.test_data['addInvestorObject'])

def test_IssuerFiatWallet():
    # Get available fiat balance
    apiTest.GET_request(f'{apiTest.domain_issuer}/accounts/fiat')
    amountBeforeDeposit = apiTest.result_JSON['available']
    # Deposit funds to fiat account
    amount = {"amount": apiTest.test_data['depositAmount']}
    apiTest.POST_request(f'{apiTest.domain_issuer}/deposit/fiat', apiTest.headers, amount)
    # Check available amount after deposit
    apiTest.GET_request(f'{apiTest.domain_issuer}/accounts/fiat')
    amountAfterDeposit = apiTest.result_JSON['available']
    apiTest.checkResponse_Parameter(amountAfterDeposit, amountBeforeDeposit + apiTest.test_data['depositAmount'])
    # Withdraw funds from fiat wallet
    apiTest.POST_request(f'{apiTest.domain_issuer}/withdraw/fiat', apiTest.headers, amount)
    # Check available amount after withdraw
    apiTest.GET_request(f'{apiTest.domain_issuer}/accounts/fiat')
    amountAfterWithdraw = apiTest.result_JSON['available']
    apiTest.checkResponse_Parameter(amountAfterWithdraw, amountAfterDeposit - apiTest.test_data['depositAmount'])
    # Get transactions list
    apiTest.GET_request(f'{apiTest.domain_issuer}/account-transactions/fiat')

def test_IssuerSettings_Team():
    # Get all issuers on whitelabel
    apiTest.GET_request(f'{apiTest.domain_issuer}/user/all')

def test_IssuerSettings_Settlements():
    # Get all settlements
    apiTest.GET_request(f'{apiTest.domain_issuer}/trade-settlement-process')
    # Get details of settlement
    apiTest.GET_request(f'{apiTest.domain_issuer}/trade-settlement-process/2')


                                                                        # Investment
def test_Investment_Person():
    # Get personal info
    apiTest.GET_request(f'{apiTest.domain_investment}/person')

def test_Investment_Marketplace():
    # Get all Marketplace projects
    apiTest.GET_request(f'{apiTest.domain_investment}/external/project/type/IVE_ONE')
    # Get Market project config
    apiTest.GET_request(f'{apiTest.domain_investment}/project/config/6/RETAIL')

def test_Investment_Dashboard():
    # Get total Invested amount
    apiTest.GET_request(f'{apiTest.domain_investment}/asset/total')
    # Get all invested Assets
    apiTest.GET_request(f'{apiTest.domain_investment}/asset/all/IVE_ONE')

def test_Investment_Orders():
    # Get all orders
    apiTest.GET_request(f'{apiTest.domain_investment}/orders')

def test_Investment_FiatWallet():
    # Get wallet transactions
    apiTest.GET_request(f'{apiTest.domain_investment}/account-transactions/fiat')
    # Get fait balance
    apiTest.GET_request(f'{apiTest.domain_investment}/accounts/fiat')