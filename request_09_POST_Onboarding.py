import requests
import json
from PageObject import API_tests



def test_9_PostOnboarding():
    apiTest = API_tests()
    apiTest.POST_request('https://investment-back.test.ive.one/person', apiTest.headers, apiTest.user_data["person"])
# with open('test_data.json') as file:
#     token_json = json.load(file)
#     idToken = token_json['idToken']


# url = "https://investment-back.test.ive.one/person"

# headers = {
#     "accept": "*/*",
#     "content-type": "application/json",
#     'Authorization': "Bearer " + idToken
# }
# person = {
#     "id": 1333,
#     "createdAt": "2023-05-28T11:31:38",
#     "updatedAt": "2023-05-28T12:01:05.521",
#     "createdBy": None,
#     "updatedBy": None,
#     "isDeleted": False,
#     "email": "cilaxo4367@favilu.com",
#     "firstName": "Artur",
#     "lastName": "Korotkyi",
#     "language": {
#         "id": 1,
#         "createdAt": "2020-05-12T00:00:00",
#         "updatedAt": None,
#         "createdBy": "admin",
#         "updatedBy": None,
#         "isDeleted": False,
#         "code": "en",
#         "name": "english"
#     },
#     "nationality": None,
#     "address": {
#         "id": 1750,
#         "createdAt": "2023-05-28T11:31:38",
#         "updatedAt": "2023-05-28T11:38:41",
#         "createdBy": None,
#         "updatedBy": None,
#         "isDeleted": False,
#         "countryCode": "UA",
#         "city": "12345",
#         "street": "ewqeq",
#         "streetNo": "1231",
#         "postalCode": "23131",
#         "phone": "+3803424242",
#         "mobile": None,
#         "website": None,
#         "email": None
#     },
#     "registrationDate": "2023-05-28T11:31:38",
#     "registrationType": None,
#     "dateOfBirth": "2000-10-02",
#     "placeOfBirth": "dafa",
#     "birthName": "46554465645654654654",
#     "maritalStatus": "SINGLE",
#     "ipAddress": None,
#     "macAddress": None,
#     "pep": False,
#     "taxResidence": None,
#     "beneficialOwner": True,
#     "selfEmployed": None,
#     "employedStatus": "SELF_EMPLOYED",
#     "profession": "QA manual",
#     "graduation": "PRIMARY",
#     "personDataType": "RETAIL",
#     "verification": None,
#     "bankDetails": None,
#     "appropriateness": {
#         "id": 354,
#         "createdAt": "2023-05-28T12:00:08",
#         "updatedAt": None,
#         "createdBy": "RuleEngine",
#         "updatedBy": None,
#         "isDeleted": False,
#         "projectId": 1,
#         "testDate": "2023-05-28T12:00:08",
#         "result": "ON_OWN_RISK",
#         "decision": False,
#         "decisionDate": None,
#         "evaluation": "Positive definition: {null=[{InvestorCategory is: not defined, user has: PRIVATE_INVESTOR, matching: true}, {InvestmentService is: [INVESTMENT_BROKERAGE], user has: [INVESTMENT_BROKERAGE, INVESTMENT_ADVICE, ASSET_MANAGEMENT], matching: true}], STO=[{TheoreticalKnowledge is: GOOD, user has: LIMITED, matching: false}, {KnowledgeAndExperienceYearsRange is: FROM_1_TO_5_YEARS, user has: FROM_1_TO_5_YEARS, matching: true}, {TradesQuantityRange is: FROM_1_TO_15_COUNT, user has: FROM_16_TO_50_COUNT, matching: true}], SHARE_OR_STOCK=[{TheoreticalKnowledge is: GOOD, user has: GOOD, matching: true}, {KnowledgeAndExperienceYearsRange is: FROM_1_TO_5_YEARS, user has: FROM_1_TO_5_YEARS, matching: true}, {TradesQuantityRange is: FROM_1_TO_15_COUNT, user has: FROM_16_TO_50_COUNT, matching: true}], FIXED_INCOME_SECURITIES=[{TheoreticalKnowledge is: GOOD, user has: GOOD, matching: true}, {KnowledgeAndExperienceYearsRange is: FROM_1_TO_5_YEARS, user has: FROM_1_TO_5_YEARS, matching: true}, {TradesQuantityRange is: FROM_1_TO_15_COUNT, user has: MORE_THAN_50_COUNT, matching: true}], PARTICIPATION_CERTIFICATE=[{TheoreticalKnowledge is: GOOD, user has: GOOD, matching: true}, {KnowledgeAndExperienceYearsRange is: FROM_1_TO_5_YEARS, user has: FROM_1_TO_5_YEARS, matching: true}, {TradesQuantityRange is: FROM_1_TO_15_COUNT, user has: FROM_1_TO_15_COUNT, matching: true}]}, Negative definition: not defined",
#         "confirm": False,
#         "path": None
#     },
#     "knowledgeAndExperienceMap": {
#         "shareOrStock": {
#             "id": 2203,
#             "createdAt": "2023-05-28T11:59:49",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "shareOrStock",
#             "theoreticalKnowledge": "GOOD",
#             "knowledgeAndExperienceYearsRange": "FROM_1_TO_5_YEARS",
#             "tradesQuantityRange": "FROM_16_TO_50_COUNT",
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": None
#         },
#         "assetManagement": {
#             "id": 2207,
#             "createdAt": "2023-05-28T12:00:06",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "assetManagement",
#             "theoreticalKnowledge": None,
#             "knowledgeAndExperienceYearsRange": None,
#             "tradesQuantityRange": None,
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": True
#         },
#         "fixedIncomeSecurities": {
#             "id": 2202,
#             "createdAt": "2023-05-28T11:59:44",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "fixedIncomeSecurities",
#             "theoreticalKnowledge": "GOOD",
#             "knowledgeAndExperienceYearsRange": "FROM_1_TO_5_YEARS",
#             "tradesQuantityRange": "MORE_THAN_50_COUNT",
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": None
#         },
#         "investmentAdvice": {
#             "id": 2206,
#             "createdAt": "2023-05-28T12:00:06",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "investmentAdvice",
#             "theoreticalKnowledge": None,
#             "knowledgeAndExperienceYearsRange": None,
#             "tradesQuantityRange": None,
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": True
#         },
#         "investmentBrokerage": {
#             "id": 2205,
#             "createdAt": "2023-05-28T12:00:06",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "investmentBrokerage",
#             "theoreticalKnowledge": None,
#             "knowledgeAndExperienceYearsRange": None,
#             "tradesQuantityRange": None,
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": True
#         },
#         "participationCertificate": {
#             "id": 2201,
#             "createdAt": "2023-05-28T11:59:33",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "participationCertificate",
#             "theoreticalKnowledge": "GOOD",
#             "knowledgeAndExperienceYearsRange": "FROM_1_TO_5_YEARS",
#             "tradesQuantityRange": "FROM_1_TO_15_COUNT",
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": None
#         },
#         "sto": {
#             "id": 2204,
#             "createdAt": "2023-05-28T11:59:55",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "financialInvestmentType": "sto",
#             "theoreticalKnowledge": "LIMITED",
#             "knowledgeAndExperienceYearsRange": "FROM_1_TO_5_YEARS",
#             "tradesQuantityRange": "FROM_16_TO_50_COUNT",
#             "amountPerYear": None,
#             "remark": None,
#             "signingDate": None,
#             "usedInThePast": None
#         }
#     },
#     "usedInPastInvestmentServices": [],
#     "documentList": [],
#     "company": None,
#     "position": None,
#     "registrationState": "{\"status\":\"progress\",\"activeStep\":{\"stepIndex\":4,\"sectionIndex\":2}}",
#     "sourceOfWealthList": [
#         {
#             "id": 501,
#             "createdAt": "2023-05-28T11:59:18",
#             "updatedAt": None,
#             "createdBy": None,
#             "updatedBy": None,
#             "isDeleted": False,
#             "source": "REAL_ESTATE_SALE",
#             "alternativeSourceOfWealth": None,
#             "amount": None
#         }
#     ],
#     "walletAddressList": [],
#     "taxInfo": {
#         "id": 461,
#         "createdAt": "2023-05-28T11:59:22",
#         "updatedAt": None,
#         "createdBy": None,
#         "updatedBy": None,
#         "isDeleted": False,
#         "taxOffice": "12314",
#         "taxNumber": "34467754",
#         "taxIdentification": "12321344132"
#     },
#     "currency": None,
#     "projects": [],
#     "isCompany": False,
#     "sammelverwahrung": False
# }

# try:
#     result = requests.post(url, headers=headers, json=person)
#     print(result)
#     # response_json = result.json()
# except:
#     raise Exception(f"Request failed. idToken could be deprecated. Response = {result.text}")

# assert result.status_code == 200


