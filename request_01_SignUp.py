import requests
import json
from PageObject import API_tests

def test_1_SignUp():
    apiTest = API_tests()
    apiTest.POST_request('https://gateway-auth.test.ive.one/auth/signUp', apiTest.user_data["signUp_header"], apiTest.user_data["newUserData"])
    # apiTest.add_token_test_data_JSON()


url = "https://gateway-auth.test.ive.one/auth/signUp"

data = {
    # "email": "gasefe9051@goflipa.com",
    "email": "a.korotkiy@ive.one",
    "firstName": "TestArt",
    "lastName": "TestUserLastname",
    "password": "Qbug.151?200",
    "countryCode": "BE",
    "terms": True,
    "policy": True,
    "language": "en",
    "registerFrom": "issuer-portal"
}

login_data = {
    "email": data["email"],
    "password": data["password"]
}

headers = {
    "accept": "*/*",
    "content-type": "application/json",
    "origin": "https://login.test.ive.one",
    "referer": "https://login.test.ive.one/"
    }

def SignUp():
    result = requests.post(url, headers=headers, json=data)
    print(result)
    print(result.text)

# if __name__ == '__main__': SignUp()

with open('test_data.json', 'w') as file:
    json.dump(login_data, file)
