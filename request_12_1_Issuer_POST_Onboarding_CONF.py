import requests
import json
from PageObject import API_tests


def test_12_PostOnboardingCONF():
    apiTest = API_tests()
    apiTest.POST_request('https://issuer-back.test.ive.one/onboarding/config', apiTest.headers, apiTest.user_data["onboardingCONF"])
# with open('test_data.json') as file:
#     token_json = json.load(file)
#     idToken = token_json['idToken']

# projectId = 243
# url = f"https://issuer-back.test.ive.one/onboarding/config"

# headers = {
#     "accept": "*/*",
#     "content-type": "application/json",
#     'Authorization': "Bearer " + idToken
# }

# onboardingCONF = {
#         "id": 516,
#         "projectId": projectId,
#         "investors": [
#             {
#                 "id": 1,
#                 "type": "RETAIL"
#             },
#             {
#                 "id": 2,
#                 "type": "PROFESSIONAL"
#             }
#         ],
#         "roles": [
#             "INVESTOR",
#             "ADVISOR",
#             "EMPLOYEE"
#         ],
#         "verificationConfig": {
#             "id": 305,
#             "defaultProvider": "POST_IDENT",
#             "countrySpecificVerificationGroup": {
#                 "id": 719,
#                 "name": None,
#                 "countrySpecificList": []
#             },
#             "verificationNotNeeded": True
#         },
#         "appropriatenessConfig": {
#             "id": 153,
#             "required": True
#         },
#         "categories": [
#             "PERSON",
#             "COMPANY"
#         ],
#         "steps": [
#             {
#                 "id": 2,
#                 "stepType": "personalCitizenStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 3,
#                 "stepType": "personalBirthStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 4,
#                 "stepType": "personalEmploymentEducationStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 6,
#                 "stepType": "personalTaxInformationStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 7,
#                 "stepType": "personalPoliticalPersonStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 8,
#                 "stepType": "personalConfirmBeneficialOwnerStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 9,
#                 "stepType": "personalParticipationRights",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 11,
#                 "stepType": "personalShareOfStock",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 13,
#                 "stepType": "personalInvestmentServicesStep",
#                 "categoryType": "PERSON"
#             },
#             {
#                 "id": 15,
#                 "stepType": "companyDetailsInfoStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 16,
#                 "stepType": "companyRegistrationStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 17,
#                 "stepType": "companyDoesCompanyDoStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 18,
#                 "stepType": "companyEmployeeCountStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 19,
#                 "stepType": "companyAddressStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 20,
#                 "stepType": "companySupportingDocumentsStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 21,
#                 "stepType": "companyTaxInformationStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 22,
#                 "stepType": "companySourceOfFundsStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 23,
#                 "stepType": "companyManagingDirectorStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 24,
#                 "stepType": "companyPositionStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 25,
#                 "stepType": "companyBeneficialOwnerSharesStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 26,
#                 "stepType": "companyEmploymentEducationStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 27,
#                 "stepType": "companyPersonalDetailsAddressStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 28,
#                 "stepType": "companyPersonalDetailsBirthStep",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 29,
#                 "stepType": "companyStakeholdersSection",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 30,
#                 "stepType": "companyParticipationRights",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 31,
#                 "stepType": "companyFixedIncome",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 32,
#                 "stepType": "companyShareOfStock",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 33,
#                 "stepType": "companySecurityToken",
#                 "categoryType": "COMPANY"
#             },
#             {
#                 "id": 34,
#                 "stepType": "companyInvestmentServicesStep",
#                 "categoryType": "COMPANY"
#             }
#         ]
    
# }

# result = requests.post(url, headers=headers, json=onboardingCONF)
# print(result.text)

# assert result.text != []
# assert result.status_code == 200


