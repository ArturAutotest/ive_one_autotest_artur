from PageObject import Find_element_click
import time

#_Issuer_Onboarding_Configuration

def test_ID8_Issuer_Onboarding_Configuration(browser):
    ive_one_main_page = Find_element_click(browser)
    ive_one_main_page.go_to_issuer()
    # ive_one_main_page.log_in_button()
    # ive_one_main_page.email_artur('a.korotkiy@ive.one')
    # ive_one_main_page.password_artur('Qbug.151?200')
    # ive_one_main_page.log_in_button()
    ive_one_main_page.Open_Autotest_project_button()
    ive_one_main_page.Settings_button()

    ive_one_main_page.Onboarding_CONF()
    ive_one_main_page.Create_button()
    #Investor details
    ive_one_main_page.Investor_button()
    ive_one_main_page.Employee_Invesor_details_button()
    ive_one_main_page.Advisor_Invesor_details_button()
    ive_one_main_page.Retail_Invesor_details_button()
    ive_one_main_page.Professional_Invesor_details_button()
    ive_one_main_page.Private_person_Invesor_details_button()
    ive_one_main_page.Company_Invesor_details_button()
    ive_one_main_page.Save_button()
    #ID Verification
    ive_one_main_page.Turn_on_button()
    ive_one_main_page.ID_Verification_Provider_button()
    ive_one_main_page.Postident_button()
    ive_one_main_page.Save_button()
    #Appropriateness Config
    ive_one_main_page.Turn_on_button()
    ive_one_main_page.Save_button()
    #Onboarding Steps(Company)
    ive_one_main_page.Company_details_button()
    ive_one_main_page.Personal_details_button()
    ive_one_main_page.Knowledge_and_experience_button()
    ive_one_main_page.Stakeholders_section_button()
    #Onboarding Steps(Personal)
    ive_one_main_page.Personal_Personal_Details_button()
    ive_one_main_page.Legal_Issues_button()
    ive_one_main_page.Knowledge_and_experience_button()
    ive_one_main_page.Save_button()
    ive_one_main_page.popup_Onboarding_was_successfully_updated_check()

    ive_one_main_page.Settings_button()
    ive_one_main_page.Onboarding_CONF()
    ive_one_main_page.Delete_button()
    ive_one_main_page.popup_Onboarding_was_successfully_deleted_check()